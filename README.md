# Telegram GPT

This is a Telegram bot that uses OpenAI's GPT-3.5 model to generate text.

## Setup

1. Create a Telegram bot using [BotFather](https://t.me/botfather).
2. Run the following:

```
poetry install
export TELEGRAM_API = "[KEY]"
export OPENAI_API_KEY = "[KEY]"
poetry run python main.py
```

## Features

- Uses `gpt-3.5-turbo`
- Keeps the state of the conversation, like ChatGPT.
- Clear the state with `/clear`
