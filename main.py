# we are writing a telegram bot that will query OpenAI's GPT API with messages from the user.
import os
import sqlite3
import datetime

import openai
import telebot

bot = telebot.TeleBot(os.environ["TELEGRAM_API"])
openai.api_key = os.environ["OPENAI_API_KEY"]
conn = sqlite3.connect("usage.db")
c = conn.cursor()

c.execute("""CREATE TABLE IF NOT EXISTS usage (
    user_id integer,
    input text,
    date text,
    time text,
    tokens integer,
    chat_id integer,
    private boolean
)""")

system_prompt= """Assistant is a large language model trained by OpenAI

Assistant is designed to be able to assist with a wide range of tasks, from answering simple questions to providing in-depth explanations and discussions on a wide range of topics. As a language model, Assistant is able to generate human-like text based on the input it receives, allowing it to engage in natural-sounding conversations and provide responses that are coherent and relevant to the topic at hand.

Assistant is constantly learning and improving, and its capabilities are constantly evolving. It is able to process and understand large amounts of text, and can use this knowledge to provide accurate and informative responses to a wide range of questions. Additionally, Assistant is able to generate its own text based on the input it receives, allowing it to engage in discussions and provide explanations and descriptions on a wide range of topics.

Overall, Assistant is a powerful tool that can help with a wide range of tasks and provide valuable insights and information on a wide range of topics. Whether you need help with a specific question or just want to have a conversation about a particular topic, Assistant is here to assist.
"""


user_messages = {}

def log_usage(user_id, input, tokens, chat_id, private):
    date = datetime.datetime.now().strftime("%d/%m/%Y")
    time = datetime.datetime.now().strftime("%H:%M:%S")

    conn = sqlite3.connect("usage.db")
    c = conn.cursor()
    c.execute("INSERT INTO usage VALUES (?, ?, ?, ?, ?, ?, ?)", (user_id, input, date, time, tokens, chat_id, private))
    conn.commit()
    c.close()
    conn.close()
    
def receive_gpt_response(message, input):
    chat_id = message.chat.id
    input_message = {"role": "user", "content": input}
    if chat_id in user_messages:
        user_messages[chat_id].append(input_message)
    else:
        user_messages[chat_id] = [input_message]

    messages = user_messages[chat_id]
    messages.append({"role": "system", "content": system_prompt})

    completion = openai.ChatCompletion.create(model="gpt-3.5-turbo", messages=user_messages[chat_id], max_tokens=800, temperature=0.8)

    response = completion.choices[0].message.content

    user_messages[chat_id].append({"role": "assistant", "content": response})

    return completion

# telegram bot functions

@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message, "Hi! I'm a Telegram bot that will echo your messages back to you.")

# add a help function that will respond by describing the commands available to this bot
@bot.message_handler(commands=['help'])
def send_help(message):
    bot.reply_to(message, "Send a message to the bot, and GPT3.5 will respond. Use /clear to clear the context of the conversation.")

@bot.message_handler(commands=['clear'])
def clear_messages(message):
    user_messages[message.from_user.id] = []

    # set message to user that context has been cleared
    bot.reply_to(message, "Context cleared.")
    
@bot.message_handler(commands=['query'])
def handle_query(message):
    prompt = message.text.replace("/query ", "")
    print("Input: " + prompt)

    chat_id = message.chat.id
    if message.chat.type == "private":
        private = True
    else:
        private = False

    completion = receive_gpt_response(message, prompt)
    response = completion.choices[0].message.content

    log_usage(message.from_user.id, prompt, completion["usage"]["total_tokens"], chat_id, private)
    print(user_messages[chat_id])
    print("Output: " + response)
    bot.reply_to(message, response)

@bot.message_handler(func=lambda message: True)
def echo_message(message):
    if message.chat.type != "private":
        return
    handle_query(message)
bot.polling()
